#!/bin/bash
rm index.md
for i in ./src/*.md;do cat $i >> tmp.md;done

export FOOTER="😍 TADX Octobre 2021"

envsubst < tmp.md > index.md

rm tmp.md

rm index.html
docker run --rm --init -v $PWD:/home/marp/app/ -e LANG=$LANG -e MARP_USER="$(id -u):$(id -g)" marpteam/marp-cli index.md --html true

node index.js
