# 30-wasi-rust

👋 Utilisation de wastime et wasmer

- cf `./hello.rs` + `build.sh`
- build
- exécuter

## How to

A standalone runtime for WebAssembly

https://github.com/bytecodealliance/wasmtime

```bash
curl https://wasmtime.dev/install.sh -sSf | bash
```


```
wasmtime 0.30.0
Wasmtime WebAssembly Runtime

USAGE:
    wasmtime <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    compile     Compiles a WebAssembly module
    config      Controls Wasmtime configuration settings
    help        Prints this message or the help of the given subcommand(s)
    run         Runs a WebAssembly module
    settings    Displays available Cranelift settings for a target
    wast        Runs a WebAssembly test script file

If a subcommand is not provided, the `run` subcommand will be used.

Usage examples:

Running a WebAssembly module with a start function:

  wasmtime example.wasm
                  
Passing command line arguments to a WebAssembly module:

  wasmtime example.wasm arg1 arg2 arg3

Invoking a specific function (e.g. `add`) in a WebAssembly module:

  wasmtime example.wasm --invoke add 1 2
```