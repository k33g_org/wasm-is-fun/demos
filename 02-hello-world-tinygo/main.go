package main

import (
	"fmt"
)

func main() {

  fmt.Println("👋 Hello World 🌍 from TinyGo 😄")
	// Prevent the function from returning, which is required in a wasm module
	<-make(chan bool)
}
