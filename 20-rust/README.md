# 20-rust

🎉 Finalement avec Rust ce n'est pas si compliqué

- cf `./hello/src/lib.rs`
- build
- cf `./hello/pkg`
- `rust.wasm.loader.js` est une version **"un peu"** modifiée de `hello_bg.js` pour fonctionner avec nodejs
- lancer `node index.js`


## How to

```bash
cargo new --lib hello
```

Add this to `Cargo.toml`:


```toml
[lib]
name = "hello"
path = "src/lib.rs"
crate-type =["cdylib"]

[dependencies]
wasm-bindgen = "0.2.50"
```

Change `./src/libs.rs`

```rust
use wasm_bindgen::prelude::*;
#[wasm_bindgen]
pub fn handle(s: String) -> String {
  let r = String::from("hello ");
  return r + &s;
}
```

```bash
cd hello; wasm-pack build
```